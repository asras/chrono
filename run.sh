#!/bin/bash
set -e


gcc -o chrono chrono.c -static

# Check if first args equals to "build"
if [ "$1" = "build" ]; then
    exit 0
fi

export CHRONO_HOME=$(pwd)
./chrono time mygame.enemy.ai
./chrono report mygame.enemy.ai
