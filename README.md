# chrono

A simple tool for tracking time spent on work sessions.

Session timings are saved as text files under $HOME/chrono_data or $CHRONO_HOME/chrono_data if $CHRONO_HOME is set.

For a test, see `run.sh`.

# Usage
## Time a work session
Time a project with a given slug (e.g. slug = "mygame.enemy.ai")
```bash
chrono time <project slug>
```
The program runs until you enter 'q' or CTRL-D. Timing is then saved. CTRL-C to abort (generate no timing).


## Generate Report 
Report a project plus subprojects with
```bash
chrono report <project slug>
```





# TODOs
- Filter on dates
- Read show_individual flag from input arg
- Show only available projects (chrono report list <project start>)
