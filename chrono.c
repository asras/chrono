#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <dirent.h>
#include <assert.h>

#define da_append(da, item) \
    do { \
        if ((da)->count >= (da)->capacity) { \
            (da)->capacity *= 2; \
            (da)->items = realloc((da)->items, (da)->capacity * sizeof((da)->items[0])); \
            assert((da)->items != NULL); \
        } \
        (da)->items[(da)->count++] = (item); \
    } while (0)


typedef struct SV {
    char *start;
    size_t len;
} SV;

SV sv_from_str(char *str) {
    SV result;
    result.start = str;
    result.len = strlen(str);
    return result;
}

void print_usage(void) {
    printf("Usage: chrono time <project>\n");
    printf("Usage: chrono report [project]\n");
}


char *chrono_format_time(time_t time) {
    struct tm *local = localtime(&time);
    char *result = malloc(100);
    sprintf(result, "%d-%02d-%02d--%02d_%02d_%02d",
            local->tm_year + 1900, local->tm_mon + 1, local->tm_mday,
            local->tm_hour, local->tm_min, local->tm_sec);
    return result;
}


typedef struct Chrono {
    time_t start;
    time_t end;
} Chrono;


void sv_copy_to_str(char *dest, SV sv) {
    memcpy(dest, sv.start, sv.len * sizeof(char));
}

void sv_copy_to(SV *dest, SV src) {
    memcpy(dest->start, src.start, src.len * sizeof(char));
    dest->len = src.len;
}

void sv_append_char(SV *sv, char c) {
    sv->start[sv->len] = c;
    sv->len++;
}


void mkdir_once(SV folder) {
    // First check if the folder already exists
    // If it does, return 
    // If it doesn't, create it;

    char folder_str[folder.len + 1];
    sv_copy_to_str(folder_str, folder);
    folder_str[folder.len] = '\0';
    struct stat st = {0};
    int success = stat(folder_str, &st);

    if (success == 0) {
        // Folder already exists
        return;
    } else if (errno == ENOENT) {
        // Folder does not exist
        int mkdir_result = mkdir(folder_str, 0777);
        if (mkdir_result != 0) {
            printf("Error creating folder '%s': %d\n", folder_str, errno);
            exit(1);
        }
    } else {
        printf("Error checking folder: %d\n", errno);
        exit(1);
    }
}


SV sv_chop_by_char(SV *sv, char c) {
    SV result;
    result.start = sv->start;
    result.len = 0;
    while (sv->len > 0) {
        if (sv->start[0] == c) {
            sv->start++;
            sv->len--;
            break;
        }
        sv->start++;
        sv->len--;
        result.len++;
    }

    return result;
}


void sv_print(SV sv) {
    for (size_t i = 0; i < sv.len; i++) {
        printf("%c", sv.start[i]);
    }
    printf("\n");
}


void sv_append_sv(SV *dest, SV src) {
    memcpy(dest->start + dest->len, src.start, src.len * sizeof(char));
    dest->len += src.len;
}

char * make_project_folder(SV chrono_home, SV project) {
    // chrono_home + '/' + project.replace(".", "/") + '\0'
    size_t folder_len = chrono_home.len + 1 + project.len + 1;
    char *backing_buffer = malloc(folder_len);
    SV folder = sv_from_str(backing_buffer);
    folder.len = 0;
    sv_copy_to(&folder, chrono_home);

    // Make chrono_home folder
    mkdir_once(folder);


    // Loop through project and create subfolders
    while (project.len > 0) {
        SV part = sv_chop_by_char(&project, '.');
        sv_append_char(&folder, '/');
        sv_append_sv(&folder, part);
        mkdir_once(folder);
    }

    char *result = malloc(folder.len + 1);
    sv_copy_to_str(result, folder);
    result[folder.len] = '\0';
    return result;
}


char *CHRONO_HOME = NULL;

char *get_chrono_home(void) {
    if (CHRONO_HOME != NULL) {
        return CHRONO_HOME;
    }

    char *chrono_home_var = getenv("CHRONO_HOME");
    if (chrono_home_var == NULL) {
        chrono_home_var = getenv("HOME");
    }
    // Home folder should be CHRONO_HOME/chrono_data
    CHRONO_HOME = malloc(strlen(chrono_home_var) + 12 + 1);
    strcpy(CHRONO_HOME, chrono_home_var);
    strcat(CHRONO_HOME, "/chrono_data");


    return CHRONO_HOME;
}

int time_command(int argc, char **argv) {
    if (argc <= 2) {
        printf("Usage: chrono time <project>\n");
        return 1;
    }

    char *chrono_home = get_chrono_home();


    // Do the timing
    Chrono chrono;
    chrono.start = time(NULL);
    { // Timing until quit
        size_t len;
        size_t buffer_len = 8;
        char buffer[buffer_len];
        while (len = read(0, buffer, buffer_len)) {
            if (buffer[0] == 'q') {
                break;
            }
        }
    }
    chrono.end = time(NULL);


    // Make sure we have the folder for this project
    char *project = argv[2];
    SV project_sv = sv_from_str(project);
    SV chrono_home_sv = sv_from_str(chrono_home);
    char *full_folder_path = make_project_folder(chrono_home_sv, project_sv);


    // Write the elapsed seconds into the file
    char *file_name = chrono_format_time(chrono.start);
    size_t elapsed_seconds = chrono.end - chrono.start;
    char *file_path = malloc(strlen(full_folder_path) + 1 + strlen(file_name) + 1);
    sprintf(file_path, "%s/%s", full_folder_path, file_name);

    FILE *file = fopen(file_path, "w");
    if (file == NULL) {
        printf("Error opening file %s: %d\n", file_path, errno);
        return 1;
    }
    fprintf(file, "%zu\n", elapsed_seconds);
    fclose(file);

    
    printf("Elapsed time: %zu seconds\n", elapsed_seconds);
    free(chrono_home);
    return 0;
}


typedef struct TimeInfo {
    char *tag;
    time_t elapsed_seconds;
} TimeInfo;

typedef struct Timings {
    TimeInfo *items;
    size_t count;
    size_t capacity;
} Timings;

typedef struct ChronoReport {
    char *name;
    Timings timings;

    struct ChronoReport *items;
    size_t count;
    size_t capacity;
} ChronoReport;


void clean_name(char *name) {
    char *p = name;
    // Skip first slash
    while (*p == '/') {
        *p = '-';
        p++;
    }

    while (*p != '\0') {
        if (*p == '/') {
            *p = '.';
        }
        p++;
    }
}

ChronoReport *gather_reports(char *folder) {
    ChronoReport *result = malloc(sizeof(ChronoReport));
    char *chrono_home = get_chrono_home();
    size_t name_len = strlen(folder) - strlen(chrono_home) + 1;
    bool is_root = name_len == 1;
    if (is_root) {
        name_len = 5;
    }
    result->name = malloc(name_len);
    if (is_root) {
        strcpy(result->name, "ROOT");
    } else {
        strcpy(result->name, folder + strlen(chrono_home));
        clean_name(result->name);
    }

    // Init timings
    result->timings.count = 0;
    result->timings.capacity = 8;
    result->timings.items = malloc(result->timings.capacity * sizeof(TimeInfo));

    // Init items
    result->count = 0;
    result->capacity = 8;
    result->items = malloc(result->capacity * sizeof(ChronoReport));




    DIR *dir = opendir(folder);
    if (dir == NULL) {
        printf("Error opening directory %s: %d\n", folder, errno);
        exit(1);
    }

    struct dirent *entry;

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_name[0] == '.') {
            continue;
        }

        switch (entry->d_type) {
            case DT_DIR: 
                {
                    // Add a subreport
                    char subfolder[strlen(folder) + 1 + strlen(entry->d_name) + 1];
                    strcpy(subfolder, folder);
                    strcat(subfolder, "/");
                    strcat(subfolder, entry->d_name);
                    ChronoReport *subreport = gather_reports(subfolder);
                    da_append(result, *subreport);
                    break;
                }
            case DT_REG:
                {
                    // Add a timing
                    // Read the file
                    // Convert content to number
                    // Make a TimeInfo
                    char filename[strlen(folder) + 1 + strlen(entry->d_name) + 1];
                    strcpy(filename, folder);
                    strcat(filename, "/");
                    strcat(filename, entry->d_name);
                    FILE *file = fopen(filename, "r");
                    if (file == NULL) {
                        printf("Error opening file %s: %d\n", filename, errno);
                        exit(1);
                    }
                    char buffer[8];
                    size_t bytes_read = fread(buffer, 1, 8, file);
                    buffer[bytes_read] = '\0';

                    int elapsed_seconds = atoi(buffer);

                    char *timing_tag = malloc(strlen(entry->d_name) + 1);
                    strcpy(timing_tag, entry->d_name);

                    TimeInfo timing = {
                        .tag = timing_tag,
                        .elapsed_seconds = elapsed_seconds,
                    };

                    da_append(&result->timings, timing);

                    fclose(file);
                    break;
                }
            default:
                {
                    fprintf(stderr, "Unknown file type for file '%s': %d\n", entry->d_name, entry->d_type);
                    break;
                }
        }
    }


    closedir(dir);

    return result;
}

size_t get_total_elapsed(ChronoReport *report) {
    // Sum up my timings
    size_t result = 0;
    for (size_t i = 0; i < report->timings.count; i++) {
        result += report->timings.items[i].elapsed_seconds;
    }
    // Sum up children timings
    for (size_t i = 0; i < report->count; i++) {
        result += get_total_elapsed(&report->items[i]);
    }
    return result;
}

void print_report(ChronoReport *report, bool show_individual) {
    size_t total_elapsed = get_total_elapsed(report);

    size_t total_hours = total_elapsed / 3600;
    size_t total_minutes = (total_elapsed % 3600) / 60;
    size_t total_seconds = total_elapsed % 60;
    printf("%s: Total %02zu:%02zu:%02zu \n", report->name, total_hours, total_minutes, total_seconds);

    if (show_individual) {
        for (int i = 0; i < report->timings.count; i++) {
            size_t timing_total_elapsed = report->timings.items[i].elapsed_seconds;
            size_t timing_total_hours = timing_total_elapsed / 3600;
            size_t timing_total_minutes = (timing_total_elapsed % 3600) / 60;
            size_t timing_total_seconds = timing_total_elapsed % 60;
            printf("  %s: %02zu:%02zu:%02zu \n", report->timings.items[i].tag, timing_total_hours, timing_total_minutes, timing_total_seconds);
        }
    }


    for (int i = 0; i < report->count; i++) {
        print_report(&report->items[i], show_individual);
    }

}


int report_command(int argc, char **argv) {
    char *chrono_home = get_chrono_home();
    char *top_folder;
    if (argc > 2) {
        char *proj_key = argv[2];
        SV proj_key_sv = sv_from_str(proj_key);
        SV chrono_home_sv = sv_from_str(chrono_home);
        top_folder = make_project_folder(chrono_home_sv, proj_key_sv);
    } else {
        top_folder = chrono_home;
    }
    ChronoReport *report = gather_reports(top_folder);


    bool show_individual = true; // todo read from argv
    print_report(report, show_individual);


    free(chrono_home);
    return 0;
}


int main(int argc, char **argv) {
    if (argc <= 1) {
        print_usage();
        return 0;
    }
    if (strcmp(argv[1], "time") == 0) {
        return time_command(argc, argv);
    } else if (strcmp(argv[1], "report") == 0) {
        return report_command(argc, argv);
    } else {
        printf("Unknown command: %s\n", argv[1]);
        print_usage();
        return 1;
    }
}
